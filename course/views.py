import json
from django.http import JsonResponse, HttpResponse
from django.views.decorators.csrf import csrf_exempt
from django.utils.decorators import method_decorator
from django.views import View

from rest_framework.decorators import api_view, authentication_classes, permission_classes
from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework import status, generics, viewsets
from .serializers import CourseSerializer
from .models import Course
from rest_framework.authentication import BasicAuthentication, SessionAuthentication, TokenAuthentication
from rest_framework.permissions import IsAuthenticated
from .permissions import IsOwnerReaderOnly

from django.contrib.auth.models import User
from django.db.models.signals import post_save
from django.dispatch import receiver
from rest_framework.authtoken.models import Token


@receiver(post_save, sender=User)
def generate_token(sender, instance=None, created=False, **kwargs):
    if created:
        Token.objects.create(user=instance)


course_dic = {
    'name': '课程名称',
    'introduction': '课程介绍',
    'price': 0.99
}


# django 原生的FBV方式处理请求
@csrf_exempt
def fbv_courses(request):
    if request.method == 'GET':
        return JsonResponse(course_dic)

    if request.method == 'POST':
        r_course = json.loads(request.body.decode('utf-8'))
        return HttpResponse(json.dumps(r_course), content_type='application/json')


@csrf_exempt
def fbv_mock_courses(request):
    if request.method == 'GET':
        return JsonResponse(course_dic)

    if request.method == 'POST':
        r_course = json.loads(request.body.decode('utf-8'))
        return HttpResponse(json.dumps(r_course), content_type='application/json')


# django 原生的CBV方式处理请求
@method_decorator(csrf_exempt, name='dispatch')
class CBVCourses(View):
    def get(self, request):
        return JsonResponse(course_dic)

    # @csrf_exempt
    def post(self, request):
        r_course = json.loads(request.body.decode('utf-8'))
        return HttpResponse(json.dumps(r_course), content_type='application/json')


# rest framework FBV
@api_view(['GET', 'POST'])
@authentication_classes((BasicAuthentication,))
@permission_classes((IsAuthenticated, ))
def drf_fbv_courses(request):
    if request.method == 'GET':
        courses = CourseSerializer(instance=Course.objects.all(), many=True)
        return Response(data=courses.data, status=status.HTTP_200_OK)

    if request.method == 'POST':
        params = CourseSerializer(data=request.data)
        if params.is_valid():
            params.save(teacher=request.user)
            return Response(params.data, status=status.HTTP_201_CREATED)
        return Response(params.errors, status=status.HTTP_400_BAD_REQUEST)


@api_view(['GET', 'PUT', 'PATCH', 'DELETE'])
def drf_fbv_course(request, pk):
    try:
        course = Course.objects.get(pk=pk)
    except Course.DoesNotExist:
        return Response(data={'msg': '查找不到记录'}, status=status.HTTP_404_NOT_FOUND)
    else:
        if request.method == 'GET':
            courses = CourseSerializer(instance=course)
            return Response(data=courses.data, status=status.HTTP_200_OK)

        if request.method == 'PUT':
            params = CourseSerializer(instance=course, data=request.data)
            if params.is_valid():
                params.save()
                return Response(params.data, status=status.HTTP_201_CREATED)
            return Response(params.errors, status=status.HTTP_400_BAD_REQUEST)

        if request.method == 'PATCH':
            params = CourseSerializer(instance=course, data=request.data, partial=True)
            if params.is_valid():
                params.save()
                return Response(params.data, status=status.HTTP_201_CREATED)
            return Response(params.errors, status=status.HTTP_400_BAD_REQUEST)

        if request.method == 'DELETE':
            course.delete()
            return Response(status=status.HTTP_204_NO_CONTENT)


# rest framework CBV
class DRFCBVCourses(APIView):
    # authentication_classes = [BasicAuthentication]
    # permission_classes = (IsAuthenticated, )

    def get(self, request):
        courses = Course.objects.all()
        r = CourseSerializer(instance=courses, many=True)
        return Response(data=r.data, status=status.HTTP_200_OK)

    def post(self, request):
        params = CourseSerializer(data=request.data)
        if params.is_valid():
            params.save(teacher=request.user)
            return Response(data=params.data, status=status.HTTP_201_CREATED)
        return Response(data=params.errors, status=status.HTTP_400_BAD_REQUEST)


class DRFCBVCourse(APIView):
    @staticmethod
    def get_obj(pk):
        try:
            return Course.objects.get(pk=pk)
        except Course.DoesNotExist:
            return

    def get(self, request, pk):
        course = DRFCBVCourse.get_obj(pk=pk)
        if not course:
            return Response(data={'msg': 'Can\'t find record'}, status=status.HTTP_404_NOT_FOUND)
        r = CourseSerializer(instance=course)
        return Response(data=r.data, status=status.HTTP_200_OK)

    def put(self, request, pk):
        course = DRFCBVCourse.get_obj(pk=pk)
        if not course:
            return Response(data={'msg': 'Can\'t find record'}, status=status.HTTP_404_NOT_FOUND)
        params = CourseSerializer(instance=course, data=request.data)
        if params.is_valid():
            params.save()
            return Response(data=params.data, status=status.HTTP_201_CREATED)
        return Response(data=params.errors, status=status.HTTP_400_BAD_REQUEST)

    def patch(self, request, pk):
        course = DRFCBVCourse.get_obj(pk=pk)
        if not course:
            return Response(data={'msg': 'Can\'t find record'}, status=status.HTTP_404_NOT_FOUND)
        params = CourseSerializer(instance=course, data=request.data, partial=True)
        if params.is_valid():
            params.save()
            return Response(data=params.data, status=status.HTTP_201_CREATED)
        return Response(data=params.errors, status=status.HTTP_400_BAD_REQUEST)

    def delete(self, request, pk):
        course = DRFCBVCourse.get_obj(pk=pk)
        if not course:
            return Response(data={'msg': 'Can\'t find record'}, status=status.HTTP_404_NOT_FOUND)
        course.delete()
        return Response(status=status.HTTP_204_NO_CONTENT)


# drf generic class base view
class GenericCourses(generics.ListCreateAPIView):
    queryset = Course.objects.all()
    serializer_class = CourseSerializer

    def perform_create(self, serializer):
        serializer.save(teacher=self.request.user)


class GenericCourse(generics.RetrieveUpdateDestroyAPIView):
    queryset = Course.objects.all()
    serializer_class = CourseSerializer
    permission_classes = (IsAuthenticated, IsOwnerReaderOnly)


# drf viewsets编写api
class ViewSetsCourse(viewsets.ModelViewSet):
    queryset = Course.objects.all()
    serializer_class = CourseSerializer

    def perform_create(self, serializer):
        serializer.save(teacher=self.request.user)
