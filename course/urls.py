from django.urls import path, include
from . import views
from rest_framework.routers import DefaultRouter


router = DefaultRouter()
router.register(prefix='drf_viewsets_router_course', viewset=views.ViewSetsCourse)

app_name = '课程'
urlpatterns = [
    path('fbv_courses', views.fbv_courses, name='django fbv api - 课程信息'),
    path('fbv_mock_courses', views.fbv_mock_courses, name='django fbv mock api - 课程信息'),
    path('cbv_mock_courses', views.CBVCourses.as_view(), name='django cbv mock api - 课程信息'),
    path('drf_fbv_courses', views.drf_fbv_courses, name='DRF fbv api - 课程信息'),
    path('drf_fbv_course/<int:pk>', views.drf_fbv_course, name='DRF fbv api - 课程详情更新删除'),
    path('drf_cbv_courses', views.DRFCBVCourses.as_view(), name='DRF cbv api - 课程信息'),
    path('drf_cbv_course/<int:pk>', views.DRFCBVCourse.as_view(), name='DRF cbv api - 课程详情更新删除'),
    path('drf_gcbv_courses', views.GenericCourses.as_view(), name='DRF gcbv api - 课程信息'),
    path('drf_gcbv_course/<int:pk>', views.GenericCourse.as_view(), name='DRF gcbv api - 课程详情更新删除'),
    path('drf_viewsets_course', views.ViewSetsCourse.as_view(
        {'get': 'list', 'post': 'create'}
    ), name='DRF viewsets api - 课程信息'),
    path('drf_viewsets_course/<int:pk>', views.ViewSetsCourse.as_view(
        {'get': 'retrieve', 'put': 'update', 'patch': 'partial_update', 'delete': 'destroy'}
    ), name='DRF viewsets api - 课程详情更新删除'),
    path('', include(router.urls))
]
